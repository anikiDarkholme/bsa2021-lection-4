﻿using Projects.Modelling.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Projects.Modelling.Entities
{
	public class ProjectEntity : ModelBase
	{
        public ProjectEntity()
        {

        }

		public ProjectEntity(Project projectModel, UserEntity authorEntity, TeamEntity teamEntity, IEnumerable<TaskEntity> taskEntities)
		{
			Id = projectModel.Id;
			UserEntity = authorEntity;
			TeamEntity = teamEntity;
			Tasks = taskEntities;
			Name = projectModel.Name;
			Description = projectModel.Description;
			Deadline = projectModel.Deadline;
			CreatedAt = projectModel.CreatedAt;
		}

	    [ForeignKey("UserEntityId")]
		public UserEntity UserEntity { get; set; }
		
		[ForeignKey("UserEntity")]
        public int? UserEntityId { get; set; }

		[ForeignKey("TeamEntityId")]
        public TeamEntity TeamEntity { get; set; }

		[ForeignKey("TeamEntity")]
        public int? TeamEntityId { get; set; }

        public IEnumerable<TaskEntity> Tasks { get; set; }

		[Column(TypeName = "nvarchar(100)")]
		public string Name { get; set; }

		[Column(TypeName = "nvarchar(300)")]
		public string Description { get; set; }

		[Column(TypeName = "datetime")]
		public DateTime? Deadline { get; set; }

		[Column(TypeName = "datetime")]
		public DateTime CreatedAt { get; set; }

		public override string ToString()
		{
			var tasksInfo = string.Empty;

			foreach (var item in Tasks)
			{
				tasksInfo += item.ToString();
			}

			return $"Id : {Id}|\n" +
				$" Author : {UserEntity.ToString()}|\n " +
				$"Team : {TeamEntity.ToString()}|\n " +
				$"Tasks : {tasksInfo}|\n" +
				$" Name : {Name}|\n " +
				$"Description : {Description}|\n " +
				$"DeadLine : {Deadline}|\n Created At : {CreatedAt}|" +
				$"User Entity Id : {UserEntityId}" + 
				$"Team Entity Id : {TeamEntityId}" +
				$"/n"; 
			}
        }
    }

