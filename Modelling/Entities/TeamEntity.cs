﻿using Projects.Modelling.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projects.Modelling.Entities
{
	public class TeamEntity : ModelBase
	{
        public TeamEntity()
        {

        }
        public TeamEntity(Team teamModel, IEnumerable<UserEntity> userEntities)
        {
			Id = teamModel.Id;
			Name = teamModel.Name;
			Users = userEntities;
			CreatedAt = teamModel.CreatedAt;
        }

		[Column(TypeName = "nvarchar(100)")]
		public string Name { get; set; }

		public IEnumerable<UserEntity> Users { get; set; }

		public IEnumerable<ProjectEntity> Projects { get; set; }

		[Column(TypeName = "datetime")]
		public DateTime CreatedAt { get; set; }

		public override string ToString()
        {
			var users = string.Empty;

			foreach (var item in users)
			{
				users += item.ToString();
			}

			return $"Id : {Id}|\n" +
				$"Users : {users}|\n" +
				$" Name : {Name}|\n " +
				$"Created At : {CreatedAt}|" +
				$"/n";
		}
	}
}

