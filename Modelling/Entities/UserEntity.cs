﻿using Projects.Modelling.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projects.Modelling.Entities
{
	public class UserEntity : ModelBase
	{
        public UserEntity()
        {

        }
        public UserEntity(User userModel)
        {
			Id = userModel.Id;
			TeamEntityId = userModel.TeamId;
			FirstName = userModel.FirstName;
			LastName = userModel.LastName;
			Email = userModel.Email;
			RegisteredAt = userModel.RegisteredAt;
			BirthDay = userModel.BirthDay;
		}

        public TeamEntity TeamEntity { get; set; }

        public int? TeamEntityId { get; set; }

		[Column(TypeName = "nvarchar(100)")]
		public string FirstName { get; set; }

		[Column(TypeName = "nvarchar(100)")]
		public string LastName { get; set; }

		[Column(TypeName = "nvarchar(100)")]
		public string Email { get; set; }

		[Column(TypeName = "datetime")]
		public DateTime RegisteredAt { get; set; }

		[Column(TypeName = "datetime")]
		public DateTime BirthDay { get; set; }

        public IEnumerable<ProjectEntity> Projects { get; set; }

        public IEnumerable<TaskEntity> Tasks { get; set; }

        public override string ToString()
        {
			return $"Id : {Id}|\n" +
				$" First Name : {FirstName}|\n " +
				$"Last Name : {LastName}|\n " +
				$"Email: {Email}|\n" +
				$"Registered At : {RegisteredAt}|\n " +
				$"Birth Day : {BirthDay}|\n " +
				$"TeamEntityId : {TeamEntityId}";
		}
	}
}

