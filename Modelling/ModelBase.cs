﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Projects.Modelling
{
    public class ModelBase
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

    }
}
