﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projects.API.Interfaces;
using Projects.API.Services;
using Projects.Modelling.Entities;
using System;
using Projects.API.ExtensionsConfiguration;

namespace Projects.API.Controllers
{
    [ApiController]
    [Route("api/tasks")]
    public class TasksController : ControllerBase
    {
        private readonly IEntityHandlerService entityHandler;
        private readonly IDTOHandlerService dtoHandler;

        public TasksController(IEntityHandlerService entityHandler, IDTOHandlerService dtoHandler)
        {
            this.entityHandler = entityHandler;
            this.dtoHandler = dtoHandler;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<TaskEntity>))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Get()
        {
            var Tasks = await entityHandler.GetAllTaskEntitiesAsync();

            if (Tasks.Count() < 1)
                return NoContent();

            return Ok(Tasks);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TaskEntity))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetByID(int id)
        {
            var Task = await entityHandler.GetTaskEntitybyIdAsync(id);

            if (Task == null)
                return NotFound();

            return Ok(Task);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(Modelling.DTOs.Task))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] Projects.Modelling.DTOs.Task Task)
        {
            string errorMessage = string.Empty;

            try
            {
                var createdTask = 
                await (dtoHandler as DTOHandlerService).TryAddTaskAsync(Task);

                    return Created("", createdTask);
            }
            catch(Exception ex)
            {
                errorMessage += ex.GetaAllMessages();
            }

            return BadRequest(errorMessage);   
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Put(int id, [FromBody] Modelling.DTOs.Task Task)
        {
            if (id < 0)
                return BadRequest("Invalid ID parameter");

            dtoHandler
                .DeleteTaskById(id);

            var addedEntity = 
            await (dtoHandler as DTOHandlerService)
                .AddTaskAsync(Task);

            return Ok(addedEntity);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Delete(int id)
        {
            if (id < 0)
                return BadRequest();

            dtoHandler
                .DeleteTaskById(id);

            return NoContent();
        }
    }
}
