﻿using Bogus;
using Projects.Modelling.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projects.API.ExtensionsConfiguration
{

	public static class SeedData
	{
		private static Faker f;

		public static List<ProjectEntity> Projects = new List<ProjectEntity>();

		public static List<TaskEntity> Tasks = new List<TaskEntity>();

		public static List<TeamEntity> Teams = new List<TeamEntity>();

		public static List<UserEntity> Users = new List<UserEntity>();

		public static void Init()
		{
			f = new Faker();

			var team1 = new TeamEntity()
			{
				Id = 1,
				CreatedAt = f.Date.Past(),
				Name = "Team1",
				Users = new List<UserEntity>(),
	

			};
			var team2 = new TeamEntity()
			{
				Id = 2,
				CreatedAt = f.Date.Past(),
				Name = "Team2",
				Users = new List<UserEntity>(),
				Projects = new List<ProjectEntity>()

			};

			var user1 = new UserEntity()
			{
				BirthDay = f.Date.Past(),
				Email = f.Internet.Email(),
				FirstName = f.Name.FirstName(),
				Id = 1,
				LastName = f.Name.LastName(),
				RegisteredAt = f.Date.Past(),
				Tasks = new List<TaskEntity>(),
				Projects = new List<ProjectEntity>(),
				
				TeamEntityId = 1,
			};
			var user2 = new UserEntity()
			{
				BirthDay = f.Date.Past(),
				Email = f.Internet.Email(),
				FirstName = f.Name.FirstName(),
				Id = 2,
				LastName = f.Name.LastName(),
				RegisteredAt = f.Date.Past(),
				Tasks = new List<TaskEntity>(),
				Projects = new List<ProjectEntity>(),
				TeamEntityId = 1
			};
			var user3 = new UserEntity()
			{
				BirthDay = f.Date.Past(),
				Email = f.Internet.Email(),
				FirstName = f.Name.FirstName(),
				Id = 3,
				LastName = f.Name.LastName(),
				RegisteredAt = f.Date.Past(),
				Tasks = new List<TaskEntity>(),
				Projects = new List<ProjectEntity>(),
				
				TeamEntityId = 2
			};
			var user4 = new UserEntity()
			{
				BirthDay = f.Date.Past(),
				Email = f.Internet.Email(),
				FirstName = f.Name.FirstName(),
				Id = 4,
				LastName = f.Name.LastName(),
				RegisteredAt = f.Date.Past(),
				Tasks = new List<TaskEntity>(),
				Projects = new List<ProjectEntity>(),
				
				TeamEntityId = 2
			};


			(team1.Users as List<UserEntity>).AddRange(new List<UserEntity>{user1, user2});
			(team2.Users as List<UserEntity>).AddRange(new List<UserEntity> { user3, user4 });


			var task1 = new TaskEntity()
			{
				Id = 1,
				CreatedAt = f.Date.Past(),
				Description = f.Lorem.Sentence(),
				Name = f.Company.CatchPhrase(),
				State = (Projects.Modelling.DTOs.State)f.Random.Number(0, 3),
				FinishedAt = f.Date.Past(),
				
				UserEntityId = 1,				
			};
			var task2 = new TaskEntity()
			{
				Id = 2,
				CreatedAt = f.Date.Past(),
				Description = f.Lorem.Sentence(),
				Name = f.Company.CatchPhrase(),
				State = (Projects.Modelling.DTOs.State)f.Random.Number(0, 3),
				FinishedAt = f.Date.Past(),
				
				UserEntityId = 1,

			};
			var task3 = new TaskEntity()
			{
				Id = 3,
				CreatedAt = f.Date.Past(),
				Description = f.Lorem.Sentence(),
				Name = f.Company.CatchPhrase(),
				State = (Projects.Modelling.DTOs.State)f.Random.Number(0, 3),
				FinishedAt = f.Date.Past(),
				
				UserEntityId = 2,
				
			};

			(user1.Tasks as List<TaskEntity>).AddRange(new List<TaskEntity> { task1, task2 });
			(user2.Tasks as List<TaskEntity>).AddRange(new List<TaskEntity> { task3});

			var project1 = new ProjectEntity()
			{
				CreatedAt = f.Date.Past(),
				Deadline = f.Date.Future(),
				Description = f.Lorem.Sentence(),
				Id = 1,
				Name = f.Company.CompanyName(),
				
				UserEntityId = 1,
				Tasks = new List<TaskEntity>(),
				
				TeamEntityId = 1
			};

			(user1.Projects as List<ProjectEntity>).AddRange(new List<ProjectEntity> { project1 });

			Teams.AddRange(new List<TeamEntity> { team1, team2 });
			Projects.Add(project1);
			Users.AddRange(new List<UserEntity> { user1, user2, user3, user4 });
			Tasks.AddRange(new List<TaskEntity> { task1, task2, task3 });
			

		}

	}
}

