using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Projects.API.ExtensionsConfiguration;
using Projects.API.Interfaces;
using Projects.API.Services;
using Projects.DataAccess;
using Projects.DataAccess.Interfaces;
using Projects.DataAccess.Repositories;
using Projects.Modelling.DTOs;
using Projects.Modelling.Interfaces;
using Projects.Modelling.Services;
using System.Configuration;
using System.Text.Json.Serialization;

namespace Projects.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IEntityBinderService, EntityBinderService>();

            services.AddAutoMapper(typeof(AutoMapperConfig));

            services.AddDbContext<ProjectsContext>(options => options.UseSqlServer(
            Configuration["ConnectionStrings:ProjectsDatabase"])
            .EnableSensitiveDataLogging());

            services.AddScoped<DbContext, ProjectsContext>();

            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<ITaskRepository, TaskRepository>();

            services.AddScoped<ITeamRepository, TeamRepository>();

            services.AddScoped<IProjectRepository, ProjectRepository>();

            services.AddScoped<IUnitOfWork, DBUnitOfWork>();
            services.AddScoped<IDTOHandlerService, DTOHandlerService>();
            services.AddScoped<IEntityHandlerService, EntityHandlerService>();
            services.AddScoped<IQueryProcessingService, QueryProcessingService>();

            services.AddControllers().AddJsonOptions(x =>
                x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ProjectsAPI", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ProjectsAPI v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
