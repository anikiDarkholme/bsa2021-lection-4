﻿using Projects.Modelling;
using System.Collections.Generic;

namespace Projects.DataAccess.Interfaces
{
    public interface IRepository<T>
    {
        T GetById(int id);

        IEnumerable<T> GetAll();

        T Add(T entity);

        void DeleteAt(int id);

        int Count();
    }
}
