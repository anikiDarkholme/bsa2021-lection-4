﻿using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projects.DataAccess.Interfaces
{
    public interface IUserRepository : IRepository<UserEntity>
    {
    }
}
