﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
    using Projects.DataAccess.Interfaces;
using Projects.Modelling;
using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    namespace Projects.DataAccess.Repositories
    {
        public class DBRepository<T> : IRepository<T> where T: ModelBase , new()
        {
            private readonly DbContext context;
            internal DbSet<T> dbSet;

            public DBRepository(DbContext context)
            {
                this.context = context;
                this.dbSet = context.Set<T>();
            }

            #region public:
            public IEnumerable<T> Query(
                 Expression<Func<T, bool>> filter,
                 params Expression<Func<T, object>>[] includeExpressions)
            {
                IQueryable<T> query = dbSet;
                if (filter != null)
                {
                    query = query.Where(filter);
                }
                foreach (var exp in includeExpressions)
                {
                    query = query.Include(exp);
                }
                return query.ToList();
            }

            public async Task<IEnumerable<T>> QueryAsync(
                 Expression<Func<T, bool>> filter,
                 params Expression<Func<T, object>>[] includeExpressions)
            {
                IQueryable<T> query = dbSet;
                if (filter != null)
                {
                    query = query.Where(filter);
                }
                foreach (var exp in includeExpressions)
                {
                    query = query.Include(exp);
                }
                return await query.ToListAsync();
            }

            public IEnumerable<T> GetAll()
            {
                return dbSet.ToList();
            }

            public async Task<IEnumerable<T>> GetAllAsync()
            {
                return await dbSet.ToListAsync();
            }

            public T GetById(int id)
            {
                if (id < 0) return new T();

                var foundEntitities =
                    Query(n => n.Id == id);

                if (foundEntitities.Count() > 1 || foundEntitities.Count() < 1)
                    return null;

                return foundEntitities.First();
            }

            public async Task<T> GetByIdAsync(int id,
               params Expression<Func<T, object>>[] includeExpressions)
            {

            if (id < 0) return null;

                var foundEntitities = await
                    QueryAsync(n => n.Id == id,
                    includeExpressions);

            if (foundEntitities.Count() > 1 || foundEntitities.Count() < 1)
                return null;

                return foundEntitities.First();
            }

            public T Add(T model)
            {
                return
                dbSet.Add(model).Entity;
            }

            public async Task<T> AddAsync(T model)
            {
                model.Id = 0;

                return
                (await dbSet.AddAsync(model)).Entity;
            }


            public void DeleteAt(int id)
            {
                var entity = GetById(id);

                if (entity != null)
                    dbSet.Remove(entity);
            }

            public void Delete(T entity)
            {
                if (entity != null)
                    dbSet.Remove(entity);
            }

            public int Count() => dbSet.Count();

            #endregion
        }
    }